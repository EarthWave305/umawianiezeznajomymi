package com.example.earthwave305.znajomi;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import java.text.ParsePosition;
import java.util.Date;

/**
 * Created by EarthWave305 on 2017-05-21.
 */

public class DatetimePickerFragment extends DialogFragment {

    String startDatetime = null;
    String endDatetime = null;
    String groupId = null;

    DatePicker datePicker;
    TimePicker timePicker;
    Button addStartDateButton;
    Button addEndDateButton;
    Button cancelButton;
    Button acceptButton;
    TextView startDateTextView;
    TextView endDateTextView;

    DbConnector dataBase;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState)
    {
        View rootView = inflater.inflate(R.layout.fragment_datetime_picker, container, false);

        datePicker = (DatePicker) rootView.findViewById(R.id.datePicker);
        timePicker = (TimePicker) rootView.findViewById(R.id.timePicker);
        addStartDateButton = (Button) rootView.findViewById(R.id.addStartDateButton);
        addEndDateButton = (Button) rootView.findViewById(R.id.addEndDateButton);
        cancelButton = (Button) rootView.findViewById(R.id.cancelButton);
        acceptButton = (Button) rootView.findViewById(R.id.acceptButton);
        startDateTextView = (TextView) rootView.findViewById(R.id.startDateTextView);
        endDateTextView = (TextView) rootView.findViewById(R.id.endDateTextView);

        dataBase = new DbConnector();

        timePicker.setIs24HourView(true);
        getDialog().setTitle("Wybierz termin");

        addStartDateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startDatetime = getDatetime();
                startDateTextView.setText("Początek terminu: " + startDatetime);
            }
        });

        addEndDateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                endDatetime = getDatetime();
                endDateTextView.setText("Koniec terminu: " + endDatetime);

            }
        });

        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        acceptButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClickAccept(v);
            }
        });

        return rootView;
    }

    public void onClickAccept(View view)
    {
        if(startDatetime == null || endDatetime == null)
            Toast.makeText(getActivity(), "Nie wybrano wszystkich terminów!", Toast.LENGTH_SHORT).show();

        else
        {
            java.text.SimpleDateFormat dateFormat = new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date start = dateFormat.parse(startDatetime, new ParsePosition(0));
            Date end = dateFormat.parse(endDatetime, new ParsePosition(0));

            if(end.before(start))
                Toast.makeText(getActivity(), "Koniec terminu nie może być przed jego początkiem!", Toast.LENGTH_SHORT).show();

            else
            {
                dataBase.query("insert into terminy (ID_grupy, ID_uzytkownika, Termin, Koniec) values(" + groupId + "," +
                        Session.getSession() + ",'" + startDatetime + "','" + endDatetime + "')");

                if(dataBase.getException())
                    Toast.makeText(getActivity(), getString(R.string.connectionError), Toast.LENGTH_SHORT).show();

                else
                {
                    dismiss();
                    String activityName = getActivity().toString().substring(0,53);

                    if(activityName.equals("com.example.earthwave305.znajomi.GroupsMemberActivity"))
                        ((GroupsMemberActivity)getActivity()).updateDatesData();

                    else
                        ((GroupsOwnerActivity)getActivity()).updateDatesData();
                    //Powyższy kod jest Januszowy z braku czasu piszącego i ozstanie poprawiony soon

                    Toast.makeText(getActivity(), "Dodano termin do grupy!", Toast.LENGTH_SHORT).show();
                }
            }
        }
    }

    public String getDatetime()
    {
        String datetimeBuffer = timePicker.getCurrentHour().toString();
        String hour = timePicker.getCurrentHour() < 10 ? "0" + datetimeBuffer : datetimeBuffer;
        datetimeBuffer = timePicker.getCurrentMinute().toString();
        String minute = timePicker.getCurrentMinute() < 10 ? "0" + datetimeBuffer : datetimeBuffer;
        String time = hour + ":" + minute + ":" + "00";

        java.text.SimpleDateFormat dateFormat = new java.text.SimpleDateFormat("yyyy-MM-dd");
        String date = dateFormat.format(new Date(datePicker.getYear()-1900, datePicker.getMonth(), datePicker.getDayOfMonth()));

        return date + " " + time;
    }

    public void setGroupId(String groupId)
    {
        this.groupId = groupId;
    }
}
