package com.example.earthwave305.znajomi;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;

/**
 * Created by EarthWave305 on 2017-04-23.
 */

public class LobbyGroupsFragment extends Fragment
{
    ArrayList<String> groupsList;
    ArrayList<String> meetingsGroupData;
    ArrayList<String> meetingsDateData;

    TextView meetingsTextView;
    ListView groupsListView;
    ListView meetingsListView;
    ImageButton refreshButton;

    DbConnector dataBase;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        final View rootView = inflater.inflate(R.layout.fragment_lobby_groups, container, false);

        meetingsTextView = (TextView) rootView.findViewById(R.id.meetingsTextView);
        meetingsListView = (ListView) rootView.findViewById(R.id.meetingsListView);
        groupsListView = (ListView) rootView.findViewById(R.id.groupsListView);
        refreshButton = (ImageButton) rootView.findViewById(R.id.refreshButton);

        dataBase = new DbConnector();
        groupsList = new ArrayList<String>(1);
        meetingsGroupData = new ArrayList<String>(1);
        meetingsDateData = new ArrayList<String>(1);

        updateGroupList();
        updateMeetingsList();

        refreshButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateGroupList();
                updateMeetingsList();
            }
        });

        groupsListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id)
            {
                String groupName = groupsListView.getItemAtPosition(position).toString();
                String groupId = dataBase.query("select ID_grupy from grupy where Nazwa_grupy='" + groupName + "'");

                if(dataBase.getException())
                    Toast.makeText(getActivity(), getString(R.string.connectionError), Toast.LENGTH_SHORT).show();

                else
                {
                    Intent intent = new Intent(getActivity(), GroupsActivity.class);
                    intent.putExtra("groupId", groupId);

                    boolean ownerCheck = false;
                    boolean memberCheck = false;

                    if(dataBase.query("select ID_zalozyciela from grupy where ID_grupy=" + groupId).equals(Session.getSession()))
                    {
                        intent.setClassName(getActivity(), getContext().getPackageName() + ".GroupsOwnerActivity");
                        ownerCheck = dataBase.getException();
                    }

                    else if(dataBase.query("select ID_uzytkownika from sklad_grupy where ID_grupy=" +
                            groupId + " and ID_uzytkownika=" + Session.getSession()) != null)
                    {
                        intent.setClassName(getActivity(), getContext().getPackageName() + ".GroupsMemberActivity");
                        memberCheck = dataBase.getException();
                    }

                    if(memberCheck || ownerCheck)
                        Toast.makeText(getActivity(), getString(R.string.connectionError), Toast.LENGTH_SHORT).show();

                    else
                        startActivity(intent);
                }
            }
        });

        return rootView;
    }

    public void updateGroupList()
    {
        groupsList = dataBase.columnQuery("select g.Nazwa_grupy from grupy g join sklad_grupy s on" +
                " g.ID_grupy=s.ID_grupy and s.ID_uzytkownika=" + Session.getSession());

        if(dataBase.getException())
            Toast.makeText(getActivity(), getString(R.string.connectionError), Toast.LENGTH_SHORT).show();

            groupsListView.setAdapter(new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, groupsList));
    }

    public void updateMeetingsList()
    {
        meetingsGroupData = dataBase.columnQuery("select g.Nazwa_grupy from grupy g join sklad_grupy s on s.ID_uzytkownika=" +
                Session.getSession() + " and g.Termin>=sysdate() and g.ID_grupy=s.ID_grupy");

        if(dataBase.getException())
            Toast.makeText(getActivity(), getString(R.string.connectionError), Toast.LENGTH_SHORT).show();

        else
        {
            meetingsDateData = dataBase.columnQuery("select g.Termin from grupy g join sklad_grupy s on s.ID_uzytkownika=" +
                    Session.getSession() + " and g.Termin>=sysdate() and g.ID_grupy=s.ID_grupy");

            if(dataBase.getException())
                Toast.makeText(getActivity(), getString(R.string.connectionError), Toast.LENGTH_SHORT).show();

            else
            {
                int length = meetingsGroupData.size();
                File cacheFile = new File(getActivity().getBaseContext().getFilesDir(), "offline2.txt");

                try
                {
                    FileOutputStream stream = new FileOutputStream(cacheFile, false);

                    for(int i = 0; i < length; i++)
                    {
                        meetingsGroupData.set(i, meetingsGroupData.get(i) + "\n" + meetingsDateData.get(i));
                        stream.write(meetingsGroupData.get(i).getBytes());
                        stream.write("\n".getBytes());
                    }

                    stream.close();
                }

                catch (Exception e)
                {
                    Toast.makeText(getActivity(), "Błąd operacji na danych!", Toast.LENGTH_SHORT).show();
                }

                meetingsListView.setAdapter(new ArrayAdapter<String>(getActivity(), R.layout.simple_list_little, R.id.text1, meetingsGroupData));
            }
        }

    }
}
