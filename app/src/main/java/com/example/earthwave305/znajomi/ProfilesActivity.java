package com.example.earthwave305.znajomi;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class ProfilesActivity extends AppCompatActivity
{
    String userId = "0";

    ArrayList<String> userData;

    TextView loginTextView;
    //TextView nameTextView;
    //TextView nameValueTextView;
    //TextView surnameTextView;
    //TextView surnameValueTextView;
    TextView mailTextView;
    TextView mailValueTextView;

    Button deleteButton;
    Button addButton;
    Button acceptButton;
    Button declineButton;

    DbConnector dataBase;

    Bundle extras;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profiles);

        userData = new ArrayList<String>(1);

        loginTextView = (TextView) findViewById(R.id.loginTextView);
        //nameTextView = (TextView) findViewById(R.id.nameTextView);
        //nameValueTextView = (TextView) findViewById(R.id.nameValueTextView);
        //surnameTextView = (TextView) findViewById(R.id.surnameTextView);
        //surnameValueTextView = (TextView) findViewById(R.id.surnameValueTextView);
        mailTextView = (TextView) findViewById(R.id.mailTextView);
        mailValueTextView = (TextView) findViewById(R.id.mailValueTextView);

        deleteButton = (Button) findViewById(R.id.deleteButton);
        addButton = (Button) findViewById(R.id.addButton);
        acceptButton = (Button) findViewById(R.id.acceptButton);
        declineButton = (Button) findViewById(R.id.declineButton);

        dataBase = new DbConnector();

        getUserId();
        checkFriendship();
        setUpInterface();
    }

    @Override
    protected void onStart() {
        super.onStart();
        getUserId();
        checkFriendship();
    }

    public void setUpInterface()
    {
        userData = dataBase.rowQuery(4, "select Login, Imie, Nazwisko, Mail from użytkownicy where ID_uzytkownika=" + userId);

        if(dataBase.getException())
            Toast.makeText(this, getString(R.string.connectionError), Toast.LENGTH_SHORT).show();

        else
        {
            loginTextView.setText(userData.get(0));
            //nameValueTextView.setText(userData.get(1));
            //surnameValueTextView.setText(userData.get(2));
            mailValueTextView.setText(userData.get(3));
        }
    }

    public void getUserId()
    {
        extras = getIntent().getExtras();

        if(extras != null)
            userId = extras.getString("userId");
    }

    public void onClickDelete(View view)
    {
        dataBase.query("delete from zaproszenia where ID_wysylajacego=" + Session.getSession() + " and ID_celu=" + userId);

        if(dataBase.getException())
            Toast.makeText(this, getString(R.string.connectionError), Toast.LENGTH_SHORT).show();

        else
        {
            Toast.makeText(this, "Anulowano zaproszenie " + userData.get(0) + " do znajomych!", Toast.LENGTH_SHORT).show();
            addButton.setVisibility(View.VISIBLE);
            deleteButton.setVisibility(View.GONE);
        }
    }

    public void onClickAdd(View view)
    {
        dataBase.query("insert into zaproszenia values (" + Session.getSession() + "," + userId + ")");

        if(dataBase.getException())
            Toast.makeText(this, getString(R.string.connectionError), Toast.LENGTH_SHORT).show();

        else
        {
            Toast.makeText(this, "Wysłano " + userData.get(0) + " zaproszenie do znajomych!", Toast.LENGTH_SHORT).show();
            addButton.setVisibility(View.GONE);
            deleteButton.setVisibility(View.VISIBLE);
        }
    }

    public void onClickDecline(View view)
    {
        dataBase.query("delete from zaproszenia where ID_wysylajacego=" + userId + " and ID_celu=" + Session.getSession());

        if(dataBase.getException())
            Toast.makeText(this, getString(R.string.connectionError), Toast.LENGTH_SHORT).show();

        else
        {
            Toast.makeText(this, "Usunięto zaproszenie użytkownika " + userData.get(0) + "!", Toast.LENGTH_SHORT).show();
            acceptButton.setVisibility(View.GONE);
            declineButton.setVisibility(View.GONE);
            addButton.setVisibility(View.VISIBLE);
        }
    }

    public void onClickAccept(View view)
    {
        dataBase.query("insert into kontakty values (" + Session.getSession() + "," + userId + ")");
        boolean first = dataBase.getException();

        dataBase.query("insert into kontakty values (" + userId + "," + Session.getSession() + ")");
        boolean second = dataBase.getException();

        dataBase.query("delete from zaproszenia where ID_wysylajacego=" + userId + " and ID_celu=" + Session.getSession());
        boolean third = dataBase.getException();

        if(first || second || third)
            Toast.makeText(this, getString(R.string.connectionError), Toast.LENGTH_SHORT).show();

        else
        {
            Toast.makeText(this, "Dodano " + userData.get(0) + " do listy znajomych!", Toast.LENGTH_SHORT).show();
            onBackPressed();
        }
    }

    public void checkFriendship()
    {
        String sent = dataBase.query("select ID_celu from zaproszenia where ID_wysylajacego=" + Session.getSession() + " and ID_celu=" + userId);
        boolean first = dataBase.getException();

        String invited = dataBase.query("select ID_wysylajacego from zaproszenia where ID_wysylajacego=" + userId + " and ID_celu=" + Session.getSession());
        boolean second = dataBase.getException();

        if(first || second)
            Toast.makeText(this, getString(R.string.connectionError), Toast.LENGTH_SHORT).show();

        else if(sent != null && invited == null)
        {
            deleteButton.setVisibility(View.VISIBLE);
            addButton.setVisibility(View.GONE);
            acceptButton.setVisibility(View.GONE);
            declineButton.setVisibility(View.GONE);
        }

        else if(invited != null && sent == null)
        {
            deleteButton.setVisibility(View.GONE);
            addButton.setVisibility(View.GONE);
            acceptButton.setVisibility(View.VISIBLE);
            declineButton.setVisibility(View.VISIBLE);
        }

        else
        {
            deleteButton.setVisibility(View.GONE);
            addButton.setVisibility(View.VISIBLE);
            acceptButton.setVisibility(View.GONE);
            declineButton.setVisibility(View.GONE);
        }
    }
}

