package com.example.earthwave305.znajomi;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

/**
 * Created by EarthWave305 on 2017-06-13.
 */

public class SearchFriendFragment extends DialogFragment implements DialogInterface.OnDismissListener
{
    DialogInterface.OnDismissListener onDismissListener;

    EditText searchEditText;
    ImageButton searchButton;
    Button cancelButton;
    ListView resultListView;
    CheckBox buddyCheckBox;
    CheckBox foreignCheckBox;

    DbConnector dataBase;

    ArrayList<String> resultData;

    boolean isGroupSearch = false;
    String groupId = null;

    String phrase = null;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState)
    {
        View rootView = inflater.inflate(R.layout.fragment_search_friends, container, false);

        searchEditText = (EditText) rootView.findViewById(R.id.searchEditText);
        searchButton = (ImageButton) rootView.findViewById(R.id.searchButton);
        cancelButton = (Button) rootView.findViewById(R.id.cancelButton);
        resultListView = (ListView) rootView.findViewById(R.id.resultListView);
        buddyCheckBox = (CheckBox) rootView.findViewById(R.id.buddyCheckBox);
        foreignCheckBox = (CheckBox) rootView.findViewById(R.id.foreignCheckBox);

        dataBase = new DbConnector();
        resultData = new ArrayList<String>(1);

        getDialog().setTitle("Katalog użytkowników");
        if(phrase != null)
        {
            searchEditText.setText(phrase);
            onClickSearch();
        }

        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        searchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClickSearch();
            }
        });

        resultListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                prepareIntent(resultListView.getItemAtPosition(position).toString());
            }
        });


        if(isGroupSearch)
            resultListView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
                @Override
                public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                    onLongClickResultListViewGroup(resultListView.getItemAtPosition(position).toString());
                    return true;
                }
            });


        return rootView;
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
        if(onDismissListener != null)
            onDismissListener.onDismiss(dialog);
    }

    public void setOnDismissListener(DialogInterface.OnDismissListener onDismissListener)
    {
        this.onDismissListener = onDismissListener;
    }

    public void onClickSearch()
    {
        phrase = searchEditText.getText().toString();
        String query = null;

        if(buddyCheckBox.isChecked() && foreignCheckBox.isChecked())
            query = "select Login from użytkownicy where Login like '%" + phrase + "%' and ID_uzytkownika!=" + Session.getSession();

        else if(buddyCheckBox.isChecked() && !foreignCheckBox.isChecked())
            query = "select u.Login from użytkownicy u join kontakty k on u.ID_uzytkownika=k.ID_kontaktu and k.ID_uzytkownika=" +
                    Session.getSession() + " and u.Login like '%" + phrase + "%'";

        else if(!buddyCheckBox.isChecked() && foreignCheckBox.isChecked())
            query = "select u.Login from użytkownicy u join kontakty k on u.ID_uzytkownika!=k.ID_kontaktu and k.ID_uzytkownika=" +
                    Session.getSession() + " and u.ID_uzytkownika!=" + Session.getSession() + " and u.Login like '%" + phrase + "%'";

        if(query != null)
        {
            resultData = dataBase.columnQuery(query);

            if(dataBase.getException())
                Toast.makeText(getActivity(), getString(R.string.connectionError), Toast.LENGTH_SHORT).show();

            else
                updateResultList();
        }

        else
            Toast.makeText(getActivity(), "Nie wybrano żadnej z opcji sortowania!", Toast.LENGTH_SHORT).show();
    }

    public void onLongClickResultListViewGroup(String newMemberLogin)
    {
        if (newMemberLogin.length() < getResources().getInteger(R.integer.loginMinLength))
            Toast.makeText(getActivity(), "Login musi mieć minimum " + getString(R.string.loginMinLength) +
                    " znaków długości!", Toast.LENGTH_SHORT).show();

        else
        {
            String newMemberId = dataBase.query("select ID_uzytkownika from użytkownicy where Login='" + newMemberLogin + "'");

            if(dataBase.getException())
                Toast.makeText(getActivity(), getString(R.string.connectionError), Toast.LENGTH_SHORT).show();

            else if(newMemberId == null || newMemberId.equals(Session.getSession()))
                Toast.makeText(getActivity(), "Błędna nazwa użytkownika!", Toast.LENGTH_SHORT).show();

            else
            {
                dataBase.query("insert into sklad_grupy values (" + groupId + "," + newMemberId + ")");

                if(dataBase.getException())
                    Toast.makeText(getActivity(), getString(R.string.connectionError), Toast.LENGTH_SHORT).show();

                else
                    Toast.makeText(getActivity(), "Dodano " + newMemberLogin + " do grupy!", Toast.LENGTH_SHORT).show();
            }
        }
    }

    public void updateResultList()
    {
        resultListView.setAdapter(new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, resultData));
    }

    public void setPhrase(String phrase)
    {
        this.phrase = phrase;
    }

    public void setIsGroupSearch(boolean isGroupSearch, String groupId)
    {
        this.isGroupSearch = isGroupSearch;
        this.groupId = groupId;
    }

    public void prepareIntent(String userLogin)
    {
        String userId = dataBase.query("select ID_uzytkownika from użytkownicy where Login='" + userLogin + "'");

        Intent intent = new Intent(getActivity(), ProfilesActivity.class);
        intent.putExtra("userId", userId);

        if(dataBase.getException())
            Toast.makeText(getActivity(), getString(R.string.connectionError), Toast.LENGTH_SHORT).show();

        else
        {
            if(userId.equals(Session.getSession()))
                intent.setClassName(getActivity(), getContext().getPackageName() + ".ProfilesOwnActivity");

            else
            {
                String isFriend = dataBase.query("select ID_uzytkownika from kontakty where ID_uzytkownika=" +
                        userId + " and ID_kontaktu=" + Session.getSession());

                if(dataBase.getException())
                    Toast.makeText(getActivity(), getString(R.string.connectionError), Toast.LENGTH_SHORT).show();

                else if(isFriend != null)
                    intent.setClassName(getActivity(), getContext().getPackageName() + ".ProfilesBuddyActivity");
            }

            startActivity(intent);
        }
    }
}
