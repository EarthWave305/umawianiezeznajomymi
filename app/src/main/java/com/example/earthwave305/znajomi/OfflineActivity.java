package com.example.earthwave305.znajomi;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class OfflineActivity extends AppCompatActivity {

    File cacheFile;
    File cacheFile2;
    final String cacheName = "offline.txt";
    final String cacheName2 = "offline2.txt";

    ImageButton addCostButton;
    EditText groupEditText;
    EditText costEditText;
    ListView costListView;
    ListView meetingsListView;
    TextView meetingsTextView;

    ArrayList<String> costBuffer;
    ArrayList<String> costData;
    ArrayList<String> meetingsData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_offline);

        addCostButton = (ImageButton) findViewById(R.id.addCostButton);
        groupEditText = (EditText) findViewById(R.id.groupEditText);
        costEditText = (EditText) findViewById(R.id.costEditText);
        costListView = (ListView) findViewById(R.id.costListView);
        meetingsListView = (ListView) findViewById(R.id.meetingsListView);
        meetingsTextView = (TextView) findViewById(R.id.meetingsTextView);

        costBuffer = new ArrayList<String>(1);
        costData = new ArrayList<String>(1);
        meetingsData = new ArrayList<String>(1);
        cacheFile = new File(getBaseContext().getFilesDir(), "offline.txt");
        cacheFile2 = new File(getBaseContext().getFilesDir(), "offline2.txt");

        costListView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener()
        {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id)
            {
                prepareCostRemoveDialog(position);
                return true;
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        readCache();
        readMeetingsCache();
    }

    public void onClickAdd(View view)
    {
        String groupName = groupEditText.getText().toString();
        String cost = costEditText.getText().toString();

        if(groupName.length() < 1 || cost.length() < 1)
            Toast.makeText(this, "Wprowadzone dane są za krótkie!", Toast.LENGTH_SHORT).show();

        else if(costBuffer.indexOf(groupName)%2 == 0)
            Toast.makeText(this, "Dodano już koszty dla tej grupy!", Toast.LENGTH_SHORT).show();

        else
        {
            costBuffer.add(groupName);
            costBuffer.add(cost);
            costData.add(groupName + "\n" + cost + "zł");
            updateCost();
            writeCache();
        }
    }

    public void writeCache()
    {
        try
        {
            FileOutputStream stream = new FileOutputStream(cacheFile, false);
            int size = costBuffer.size();

            for (int i = 0; i < size; i++)
            {
                stream.write(costBuffer.get(i).getBytes());
                stream.write("\n".getBytes());
            }

            stream.close();
        }

        catch (Exception e)
        {
            Toast.makeText(this, "Błąd operacji na danych!", Toast.LENGTH_SHORT).show();
        }
    }

    public void readCache()
    {
        try
        {
            InputStream in = new FileInputStream(cacheFile);
            BufferedReader reader = new BufferedReader(new InputStreamReader(in));
            String line;
            costBuffer.clear();

            while ((line = reader.readLine()) != null)
                costBuffer.add(line);

            reader.close();

            int size = costBuffer.size();
            costData.clear();

            for(int i = 0; i < size; i++, i++)
                costData.add(costBuffer.get(i) + "\n" + costBuffer.get(i+1) + " zł");

            updateCost();
        }

        catch (Exception e)
        {
            Toast.makeText(this, "Błąd odczytu!", Toast.LENGTH_SHORT).show();
        }
    }

    public void readMeetingsCache()
    {
        try
        {
            InputStream in = new FileInputStream(cacheFile2);
            BufferedReader reader = new BufferedReader(new InputStreamReader(in));
            String line;
            meetingsData.clear();

            int counter = 0;

            while ((line = reader.readLine()) != null)
            {
                if(counter % 2 == 0)
                    meetingsData.add(line);

                else
                    meetingsData.set(counter - 1, meetingsData.get(counter - 1) + "\n" + line);

                counter++;
            }

            reader.close();

            meetingsListView.setAdapter(new ArrayAdapter<String>(this, R.layout.simple_list_little, R.id.text1, meetingsData));
        }

        catch (Exception e)
        {
            Toast.makeText(this, "Błąd odczytu!", Toast.LENGTH_SHORT).show();
        }
    }

    public void updateCost()
    {
        costListView.setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, costData));
    }

    public void prepareCostRemoveDialog(final int position)
    {
        final AlertDialog costRemoveDialog = new AlertDialog.Builder(OfflineActivity.this).create();
        costRemoveDialog.setMessage("Czy chcesz usunąć koszt: " + costListView.getItemAtPosition(position) + " ?");

        costRemoveDialog.setButton(Dialog.BUTTON_POSITIVE, "TAK", new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialog, int which)
            {
                costData.remove(position);
                costBuffer.remove(2*position+1);
                costBuffer.remove(2*position);
                writeCache();
                updateCost();

                costRemoveDialog.dismiss();
                Toast.makeText(getApplication(), "Usunięto koszt!", Toast.LENGTH_SHORT).show();
            }
        });

        costRemoveDialog.setButton(Dialog.BUTTON_NEGATIVE, "NIE", new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialog, int which)
            {
                costRemoveDialog.dismiss();
            }
        });

        costRemoveDialog.show();
    }
}

