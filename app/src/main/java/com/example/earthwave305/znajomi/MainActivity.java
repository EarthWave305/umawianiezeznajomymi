package com.example.earthwave305.znajomi;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity
{
    Button registerButton; //deklaracja zzmiennych
    Button loginButton;
    TextView loginTextView;
    TextView passTextView;
    EditText loginEditText;
    EditText passEditText;

    DbConnector dataBase; //lacznik z baza danych (autorskiej klasy Tomasza G.)

    @Override
    protected void onCreate(Bundle savedInstanceState)

    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        registerButton = (Button)findViewById(R.id.registerButton); //Inicjalizacja wszystkich zmiennych
        loginButton = (Button)findViewById(R.id.loginButton);
        loginTextView = (TextView)findViewById(R.id.loginTextView);
        passTextView = (TextView) findViewById(R.id.passTextView);
        loginEditText = (EditText) findViewById(R.id.loginEditText);
        passEditText = (EditText) findViewById(R.id.passEditText);

        dataBase = new DbConnector();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings)
        {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void onClickLogin(View v) //procedura logowania z uzyciem bazy danych
    {
        String login = loginEditText.getText().toString(); //pobranie danych z okna tekstowego login
        String pass = passEditText.getText().toString();
        int loginMinLength = getResources().getInteger(R.integer.loginMinLength); //pobranie minimalnej dlugosci hasel, itp z integers.xml
        int passMinLength = getResources().getInteger(R.integer.passMinLength);

        if(login.length() >= loginMinLength && pass.length() >= passMinLength) //spr. czy login i haslo maja 'minLength' znakow zeby nie obciazac bazy
        {
            String userId = dataBase.query("select ID_uzytkownika from użytkownicy where Login='" + login + "' and Haslo='" + pass + "'");
            //warunek spelniony wiec sprawdzamy w bazie czy uzytkownik istnieje

            //Jesli zapytanie SQL zwroci '?' oznacza to blad polaczenia lub niepoprawna skladnie
            if(dataBase.getException())
                Toast.makeText(getApplicationContext(), getString(R.string.connectionError), Toast.LENGTH_SHORT).show();

            else if(userId != null) //jesli uzytkownik istnieje to logujemy
            {
                Intent intent = new Intent(this, LobbyActivity.class);
                Session.setSession(userId); //ustawienie statycznej zmiennej na Id zalogowanego użytkownika
                startActivity(intent);
            }

            else //jesli uzytkownik nie istnieje to sorry :D
                Toast.makeText(getApplicationContext(), "Niepoprawne hasło lub login!", Toast.LENGTH_SHORT).show();
        }

        else if(login.length() < loginMinLength)
            Toast.makeText(getApplicationContext(), "Login musi mieć minimum " + getString(R.string.loginMinLength) + " znaków długości!",
                    Toast.LENGTH_SHORT).show();
        else
            Toast.makeText(getApplicationContext(), "Hasło musi mieć minimum " + getString(R.string.passMinLength) + " znaków długości!",
                    Toast.LENGTH_SHORT).show();

    }

    public void onClickRegister(View v) //Włącza aktywnosc rejestracji
    {
        Intent intent = new Intent(this, RegisterActivity.class);
        startActivity(intent);
    }

    public void onClickOffline(View view)
    {
        Intent intent = new Intent(this, OfflineActivity.class);
        startActivity(intent);
    }

    public void clearEditTexts() //Czyszczenie edit boxow
    {
        loginEditText.setText("");
        passEditText.setText("");
    }
}
