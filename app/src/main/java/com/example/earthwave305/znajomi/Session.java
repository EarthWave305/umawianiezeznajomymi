package com.example.earthwave305.znajomi;

/**
 * Created by EarthWave305 on 2017-05-05.
 */

public class Session
{

    static String loggedUserId = "0";

    static public String getSession()
    {
        return loggedUserId;
    }

    static public void setSession(String loggedUserId)
    {
        Session.loggedUserId = loggedUserId;
    }
}
