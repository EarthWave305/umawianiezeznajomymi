package com.example.earthwave305.znajomi;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.TextView;

public class EventScannerActivity extends AppCompatActivity {

    TextView rangeTextView;
    TextView rangeValueTextView;
    Button scanButton;
    SeekBar rangeSeekBar;

    Bundle extras;

    public void GenerateMap(View v)
    {
        Intent intent = new Intent(this, EventMapActivity.class);
        startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event_scanner);

        rangeTextView = (TextView) findViewById(R.id.rangeTextView);
        rangeValueTextView = (TextView) findViewById(R.id.rangeValueTextView);
        scanButton = (Button) findViewById(R.id.scanButton);
        rangeSeekBar = (SeekBar) findViewById(R.id.rangeSeekBar);

        extras = getIntent().getExtras();

        if(extras != null)
        {
            scanButton.setText(extras.getString("id")); //TESTOWE DO EXTRA
        }

        rangeSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser)
            {
                rangeValueTextView.setText(progress + " km");
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

    }
}
