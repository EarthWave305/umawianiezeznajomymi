package com.example.earthwave305.znajomi;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class ProfilesBuddyActivity extends AppCompatActivity
{
    String userId = "0";

    ArrayList<String> userData;

    TextView loginTextView;
    TextView nameTextView;
    TextView nameValueTextView;
    TextView surnameTextView;
    TextView surnameValueTextView;
    TextView mailTextView;
    TextView mailValueTextView;

    Button deleteButton;

    DbConnector dataBase;

    Bundle extras;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profiles_buddy);

        userData = new ArrayList<String>(1);

        loginTextView = (TextView) findViewById(R.id.loginTextView);
        nameTextView = (TextView) findViewById(R.id.nameTextView);
        nameValueTextView = (TextView) findViewById(R.id.nameValueTextView);
        surnameTextView = (TextView) findViewById(R.id.surnameTextView);
        surnameValueTextView = (TextView) findViewById(R.id.surnameValueTextView);
        mailTextView = (TextView) findViewById(R.id.mailTextView);
        mailValueTextView = (TextView) findViewById(R.id.mailValueTextView);

        deleteButton = (Button) findViewById(R.id.deleteButton);

        dataBase = new DbConnector();

        getUserId();
        setUpInterface();
    }

    @Override
    protected void onStart() {
        super.onStart();
        getUserId();
    }

    public void setUpInterface()
    {
        userData = dataBase.rowQuery(4, "select Login, Imie, Nazwisko, Mail from użytkownicy where ID_uzytkownika=" + userId);

        if(dataBase.getException())
            Toast.makeText(this, getString(R.string.connectionError), Toast.LENGTH_SHORT).show();

        else
        {
            loginTextView.setText(userData.get(0));
            nameValueTextView.setText(userData.get(1));
            surnameValueTextView.setText(userData.get(2));
            mailValueTextView.setText(userData.get(3));
        }
    }

    public void getUserId()
    {
        extras = getIntent().getExtras();

        if(extras != null)
            userId = extras.getString("userId");
    }

    public void onClickDelete(View view)
    {
        dataBase.query("delete from kontakty where ID_uzytkownika=" + Session.getSession() + " and ID_kontaktu=" + userId);

        if(dataBase.getException())
            Toast.makeText(this, getString(R.string.connectionError), Toast.LENGTH_SHORT).show();

        else
        {
            dataBase.query("delete from kontakty where ID_uzytkownika=" + userId + " and ID_kontaktu=" + Session.getSession());

            if(dataBase.getException())
                Toast.makeText(this, getString(R.string.connectionError), Toast.LENGTH_SHORT).show();

            else
            {
                Toast.makeText(this, "Usunięto użytkownika " + userData.get(0) + " ze znajomych!", Toast.LENGTH_SHORT).show();
                onBackPressed();
            }
        }
    }
}

