package com.example.earthwave305.znajomi;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class GroupsActivity extends AppCompatActivity
{
    String groupId = "0";

    ArrayList<String> groupData;
    //ArrayList<String> membersData;

    //ListView membersListView;
    TextView groupNameTextView;
    TextView ownerTextView;
    TextView ownerValueTextView;
    TextView interestTextView;
    TextView interestValueTextView;
    TextView dateTextView;
    TextView dateValueTextView;

    DbConnector dataBase;

    Bundle extras;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_groups);

        getGroupId();

        groupData = new ArrayList<String>(1);
        //membersData = new ArrayList<String>(1);

        dataBase = new DbConnector();

        //membersListView = (ListView)  findViewById(R.id.membersListView);
        groupNameTextView = (TextView) findViewById(R.id.groupNameTextView);
        ownerTextView = (TextView) findViewById(R.id.ownerTextView);
        ownerValueTextView = (TextView) findViewById(R.id.ownerValueTextView);
        interestTextView = (TextView) findViewById(R.id.interestTextView);
        interestValueTextView = (TextView) findViewById(R.id.interestValueTextView);
        dateTextView = (TextView) findViewById(R.id.dateTextView);
        dateValueTextView = (TextView) findViewById(R.id.dateValueTextView);

        setUpInterface();
    }

    @Override
    protected void onStart()
    {
        super.onStart();
        getGroupId();
    }

    public void getGroupId()
    {
        extras = getIntent().getExtras();

        if(extras != null)
            groupId = extras.getString("groupId");
    }

    public void setUpInterface()
    {
        groupData = dataBase.rowQuery(4, "select Nazwa_grupy, ID_zalozyciela, ID_zainteresowania, Termin from grupy where ID_grupy=" + groupId);

        if(dataBase.getException())
            Toast.makeText(this, getString(R.string.connectionError), Toast.LENGTH_SHORT).show();

        else
        {
            String interestValue = dataBase.query("select Nazwa_zainteresowania from zainteresowania where ID_zainteresowania=" + groupData.get(2));
            boolean interestException = dataBase.getException();

            String ownerValue = dataBase.query("select Login from użytkownicy where ID_uzytkownika=" + groupData.get(1));
            boolean ownerException = dataBase.getException();

            /*membersData = dataBase.columnQuery("select u.Login from użytkownicy u join sklad_grupy s on " +
                    "u.ID_uzytkownika=s.ID_uzytkownika and s.ID_grupy=" + groupId);*/
            boolean membersException = dataBase.getException();

            if(ownerException || interestException || membersException)
                Toast.makeText(this, getString(R.string.connectionError), Toast.LENGTH_SHORT).show();

            else
            {
                groupNameTextView.setText(groupData.get(0));
                ownerValueTextView.setText(ownerValue);
                interestValueTextView.setText(interestValue);
                dateValueTextView.setText(groupData.get(3));

                /*if(!membersData.isEmpty())
                    membersListView.setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, membersData));*/
            }
        }
    }
}
