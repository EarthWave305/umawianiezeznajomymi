package com.example.earthwave305.znajomi;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class RegisterActivity extends AppCompatActivity
{

    Button registerButton; //deklaracje zmiennych
    Button backButton;
    TextView loginTextView;
    TextView passTextView;
    TextView mailTextView;
    TextView nameTextView;
    TextView surnameTextView;

    EditText loginEditText;
    EditText passEditText;
    EditText mailEditText;
    EditText nameEditText;
    EditText surnameEditText;

    DbConnector dataBase; //lacznik z baza danych

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        registerButton = (Button)findViewById(R.id.registerButton); //Inicjalizacja zmiennych
        backButton = (Button)findViewById(R.id.backButton);

        loginTextView = (TextView)findViewById(R.id.loginTextView);
        passTextView = (TextView) findViewById(R.id.passTextView);
        mailTextView = (TextView) findViewById(R.id.mailTextView);
        nameTextView = (TextView) findViewById(R.id.nameTextView);
        surnameTextView = (TextView) findViewById(R.id.surnameTextView);

        loginEditText = (EditText) findViewById(R.id.loginEditText);
        passEditText = (EditText) findViewById(R.id.passEditText);
        mailEditText = (EditText) findViewById(R.id.mailEditText);
        nameEditText = (EditText) findViewById(R.id.nameEditText);
        surnameEditText = (EditText) findViewById(R.id.surnameEditText);
        dataBase = new DbConnector();
    }

    public void onClickRegister(View view) //Procedura rejestracji wywolana w momencie klikniecie "Rejestruj"
    {
        String login = loginEditText.getText().toString(); //pobieramy wprowadzone przez uzytkownika dane
        String pass = passEditText.getText().toString();
        String mail = mailEditText.getText().toString();
        String name = nameEditText.getText().toString();
        String surname = surnameEditText.getText().toString();

        boolean loginCorrectness = checkCorrectness(login, getResources().getInteger(R.integer.loginMinLength));
        boolean passCorrectness = checkCorrectness(pass, getResources().getInteger(R.integer.passMinLength));
        boolean mailCorrectness = checkCorrectness(mail, getResources().getInteger(R.integer.mailMinLength));
        boolean nameCorrectness = checkCorrectness(name, getResources().getInteger(R.integer.nameMinLength));
        boolean surnameCorrectness = checkCorrectness(surname, getResources().getInteger(R.integer.surnameMinLength));

        if(!loginCorrectness || !passCorrectness || !mailCorrectness || !nameCorrectness || !surnameCorrectness) //jesli dlugosc ktoregokolwiek elementu jest za mala to wyswietlamy to
            Toast.makeText(getApplicationContext(), "Wprowadzone dane mają nieprawidłową długość", Toast.LENGTH_SHORT).show();

        else //dopiero jesli dlugosci sie zgadzaja to zapedzamy do dzialania baze zeby jej bez sensu nie obciazac
        {
            String loginAvailability = dataBase.query("select ID_uzytkownika from użytkownicy where Login='" + login + "'");
            boolean loginException = dataBase.getException();
            String mailAvailability = dataBase.query("select ID_uzytkownika from użytkownicy where Mail='" + mail + "'");
            boolean mailException = dataBase.getException();
            // ^sprawdzamy czy w bazie jest juz mail/login wpisany przez uzytkownika
            //dwie zmienne bool maja za zadanie wylapac czy wystapily wyjatki w odpowiadajacych im zapytaniach

            //Jesli w choc jednym zapytaniu wystapil wyjatek to mamy blad polaczenia/skladni
            if(loginException || mailException)
                Toast.makeText(getApplicationContext(), getString(R.string.connectionError), Toast.LENGTH_SHORT).show();

            // Ponizsze instrukcje w zaleznosci od tego co jest zajete, a co nie, wyswietlaja komunikaty
            else if(loginAvailability != null && mailAvailability != null)
                Toast.makeText(getApplicationContext(), "Wybrany login i e-mail są już zajęte!", Toast.LENGTH_SHORT).show();

            else if(loginAvailability != null)
                Toast.makeText(getApplicationContext(), "Wybrany login jest już zajęty!", Toast.LENGTH_SHORT).show();

            else if(mailAvailability != null)
                Toast.makeText(getApplicationContext(), "Wybrany e-mail jest już zajęty!", Toast.LENGTH_SHORT).show();

            else //Jesli login i mail sa wolne to wprowadzamy dane uzytkownika do bazy
            {
                String result = dataBase.query("insert into użytkownicy (Imie, Nazwisko, Mail, Login, Haslo) values('"
                        + name + "','" + surname + "','" + mail + "','" + login + "','" + pass + "')");

                if(dataBase.getException()) //jesli w zapytaniu SQL wystapil wyjatek to mamy blad polaczenia/skladni
                    Toast.makeText(getApplicationContext(), getString(R.string.connectionError), Toast.LENGTH_SHORT).show();

                else
                {
                    clearEditTexts();
                    Toast.makeText(getApplicationContext(), "Zarejestrowano!", Toast.LENGTH_SHORT).show();
                }
            }
        }
    }

    public void onClickBack(View view) //Procedura powrotu do ekranu logowania
    {
        clearEditTexts();
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }

    public boolean checkCorrectness(String string, int minLength) //Sprawdzanie poprawnej dlugosci elementow
    {
        boolean correctness = true;

        if(string.length() < minLength)
            correctness = false;

        return correctness;
    }

    public void clearEditTexts() //Czyszczenie edit boxow
    {
        loginEditText.setText("");
        passEditText.setText("");
        mailEditText.setText("");
        nameEditText.setText("");
        surnameEditText.setText("");
    }
}
