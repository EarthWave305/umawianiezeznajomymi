package com.example.earthwave305.znajomi;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.InputType;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class GroupsMemberActivity extends AppCompatActivity
{
    String groupId = "0";

    ArrayList<String> groupData;
    ArrayList<String> membersData;
    ArrayList<String> datesData;

    ListView membersListView;
    ListView datesListView;
    TextView groupNameTextView;
    TextView ownerTextView;
    TextView ownerValueTextView;
    TextView interestTextView;
    TextView interestValueTextView;
    TextView dateTextView;
    TextView dateValueTextView;
    TextView costTextView;
    TextView costValueTextView;
    ImageButton addMemberButton;
    EditText addMemberEditText;
    ImageButton refreshButton;
    Button leaveButton;
    Button addDateButton;
    Button costManageButton;

    DbConnector dataBase;

    Bundle extras;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_groups_member);

        getGroupId();

        groupData = new ArrayList<String>(1);
        membersData = new ArrayList<String>(1);
        datesData = new ArrayList<String>(1);

        dataBase = new DbConnector();

        membersListView = (ListView) findViewById(R.id.membersListView);
        datesListView = (ListView) findViewById(R.id.datesListView);
        groupNameTextView = (TextView) findViewById(R.id.groupNameTextView);
        ownerTextView = (TextView) findViewById(R.id.ownerTextView);
        ownerValueTextView = (TextView) findViewById(R.id.ownerValueTextView);
        interestTextView = (TextView) findViewById(R.id.interestTextView);
        interestValueTextView = (TextView) findViewById(R.id.interestValueTextView);
        dateTextView = (TextView) findViewById(R.id.dateTextView);
        dateValueTextView = (TextView) findViewById(R.id.dateValueTextView);
        costTextView = (TextView) findViewById(R.id.costTextView);
        costValueTextView = (TextView) findViewById(R.id.costValueTextView);
        addMemberButton = (ImageButton) findViewById(R.id.addMemberButton);
        refreshButton = (ImageButton) findViewById(R.id.refreshButton);
        addMemberEditText = (EditText) findViewById(R.id.addMemberEditText);
        leaveButton = (Button) findViewById(R.id.leaveButton);
        addDateButton = (Button) findViewById(R.id.addDateButton);
        costManageButton = (Button) findViewById(R.id.costManageButton);

        membersListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                prepareIntent(membersListView.getItemAtPosition(position).toString());
            }
        });

        datesListView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                prepareDateRemoveDialog(datesListView.getItemAtPosition(position).toString());
                return true;
            }
        });

        setUpInterface();
    }

    @Override
    protected void onStart()
    {
        super.onStart();
        getGroupId();
    }

    public void getGroupId()
    {
        extras = getIntent().getExtras();

        if(extras != null)
            groupId = extras.getString("groupId");
    }

    public void setUpInterface()
    {
        groupData = dataBase.rowQuery(4, "select Nazwa_grupy, ID_zalozyciela, ID_zainteresowania, Termin from grupy where ID_grupy=" + groupId);

        if(dataBase.getException())
            Toast.makeText(this, getString(R.string.connectionError), Toast.LENGTH_SHORT).show();

        else
        {
            String interestValue = dataBase.query("select Nazwa_zainteresowania from zainteresowania where ID_zainteresowania=" + groupData.get(2));
            boolean interestException = dataBase.getException();

            String ownerValue = dataBase.query("select Login from użytkownicy where ID_uzytkownika=" + groupData.get(1));
            boolean ownerException = dataBase.getException();

            membersData = dataBase.columnQuery("select u.Login from użytkownicy u join sklad_grupy s on " +
                    "u.ID_uzytkownika=s.ID_uzytkownika and s.ID_grupy=" + groupId);
            boolean membersException = dataBase.getException();

            if(ownerException || interestException || membersException)
                Toast.makeText(this, getString(R.string.connectionError), Toast.LENGTH_SHORT).show();

            else
            {
                groupNameTextView.setText(groupData.get(0));
                ownerValueTextView.setText(ownerValue);
                interestValueTextView.setText(interestValue);
                dateValueTextView.setText(groupData.get(3));

                membersListView.setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, membersData));
                updateDatesData();
            }
        }
    }

    public void updateDatesData()
    {
        datesData = dataBase.columnQuery("select Termin from terminy where ID_uzytkownika=" + Session.getSession() + " and ID_grupy=" + groupId);

        if(dataBase.getException())
            Toast.makeText(this, getString(R.string.connectionError), Toast.LENGTH_SHORT).show();

        else
        {
            ArrayList<String> buffer = new ArrayList<String>(1);
            buffer = dataBase.columnQuery("select Koniec from terminy where ID_uzytkownika=" + Session.getSession() + " and ID_grupy=" + groupId);

            if(dataBase.getException())
                Toast.makeText(this, getString(R.string.connectionError), Toast.LENGTH_SHORT).show();

            else
            {
                for(int i = 0; i < buffer.size(); i++)
                    datesData.set(i, datesData.get(i).substring(0,16) + " - " + buffer.get(i).substring(0,16));

                datesListView.setAdapter(new ArrayAdapter<String>(this, R.layout.simple_list_little, R.id.text1, datesData));
            }
        }
    }

    public void onClickRefresh(View view) {setUpInterface();}

    public void onClickLeave(View view)
    {
        dataBase.query("delete from sklad_grupy where ID_grupy=" + groupId + " and ID_uzytkownika=" + Session.getSession());

        if(dataBase.getException())
            Toast.makeText(this, getString(R.string.connectionError), Toast.LENGTH_SHORT).show();

        else
            onBackPressed();
    }

    public void onClickAddDate(View view)
    {
        DatetimePickerFragment datetime = new DatetimePickerFragment();
        datetime.setGroupId(groupId);
        FragmentManager fm = getSupportFragmentManager();
        datetime.show(fm, "asa");
    }

    public void onClickManageCost(View view)
    {
        String cost = dataBase.query("select Koszty from Koszty where ID_uzytkownika=" + Session.getSession() + " and ID_grupy=" + groupId);

        if(dataBase.getException())
            Toast.makeText(getApplication(), getString(R.string.connectionError), Toast.LENGTH_SHORT).show();

        else
        {
            final AlertDialog costDialog = new AlertDialog.Builder(GroupsMemberActivity.this).create();
            costDialog.setTitle("Twoje koszty");

            if(cost == null)
                costDialog.setMessage("Aktualne koszty: Brak");

            else
                costDialog.setMessage("Aktualne koszty: " + cost + " zł");

            final EditText input = new EditText(GroupsMemberActivity.this);
            input.setHint("Wprowadź swoje koszty");
            input.setInputType(InputType.TYPE_CLASS_NUMBER);
            costDialog.setView(input);

            costDialog.setButton(Dialog.BUTTON_POSITIVE, "Zmień", new DialogInterface.OnClickListener()
            {
                @Override
                public void onClick(DialogInterface dialog, int which)
                {
                    String costNew = input.getText().toString();

                        String checkCost = dataBase.query("select ID_uzytkownika from Koszty where ID_uzytkownika=" + Session.getSession() +
                            " and ID_grupy=" + groupId);

                        if(dataBase.getException())
                            Toast.makeText(getApplication(), getString(R.string.connectionError), Toast.LENGTH_SHORT).show();

                        else
                        {
                            if(checkCost == null)
                            {
                                dataBase.query("insert into Koszty values(" + Session.getSession() + "," + groupId + "," + costNew + ")");

                                if(dataBase.getException())
                                    Toast.makeText(getApplication(), getString(R.string.connectionError), Toast.LENGTH_SHORT).show();

                                else
                                {
                                    costDialog.dismiss();
                                    Toast.makeText(getApplication(), "Zaktualizowano koszty! Nowy koszt: " + costNew + " zł", Toast.LENGTH_SHORT).show();
                                }
                            }

                            else
                            {
                                dataBase.query("update Koszty set Koszty=" + costNew + " where ID_uzytkownika=" + Session.getSession() +
                                    " and ID_grupy=" + groupId);

                                if(dataBase.getException())
                                    Toast.makeText(getApplication(), getString(R.string.connectionError), Toast.LENGTH_SHORT).show();

                                else
                                {
                                    costDialog.dismiss();
                                    Toast.makeText(getApplication(), "Zaktualizowano koszty! Nowy koszt: " + costNew + " zł", Toast.LENGTH_SHORT).show();
                                }
                            }
                        }
                }
            });

            costDialog.setButton(Dialog.BUTTON_NEGATIVE, "Synchronizuj", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which)
                {
                    File cacheFile = new File(getBaseContext().getFilesDir(), "offline.txt");
                    ArrayList<String> costBuffer = new ArrayList<String>(1);

                    try
                    {
                        InputStream in = new FileInputStream(cacheFile);
                        BufferedReader reader = new BufferedReader(new InputStreamReader(in));
                        String line;

                        while ((line = reader.readLine()) != null)
                            costBuffer.add(line);

                        reader.close();
                    }

                    catch (Exception e)
                    {
                        Toast.makeText(getApplication(), "Błąd odczytu!", Toast.LENGTH_SHORT).show();
                    }

                    String costNew = costBuffer.indexOf(groupData.get(0)) == -1 ? null : costBuffer.get(costBuffer.indexOf(groupData.get(0)) + 1);

                    if(costNew == null)
                    {
                        Toast.makeText(getApplication(), "Nie można synchronizować! Brak danych dla grupy w bazie lokalnej!", Toast.LENGTH_SHORT).show();
                        costDialog.dismiss();
                    }

                    else
                    {
                        String checkCost = dataBase.query("select ID_uzytkownika from Koszty where ID_uzytkownika=" + Session.getSession() +
                                " and ID_grupy=" + groupId);

                        if(dataBase.getException())
                            Toast.makeText(getApplication(), getString(R.string.connectionError), Toast.LENGTH_SHORT).show();

                        else
                        {
                            if(checkCost == null)
                            {
                                dataBase.query("insert into Koszty values(" + Session.getSession() + "," + groupId + "," + costNew + ")");

                                if(dataBase.getException())
                                    Toast.makeText(getApplication(), getString(R.string.connectionError), Toast.LENGTH_SHORT).show();

                                else
                                {
                                    costDialog.dismiss();
                                    Toast.makeText(getApplication(), "Zaktualizowano koszty! Nowy koszt: " + costNew + " zł", Toast.LENGTH_SHORT).show();
                                }
                            }

                            else
                            {
                                dataBase.query("update Koszty set Koszty=" + costNew + " where ID_uzytkownika=" + Session.getSession() +
                                        " and ID_grupy=" + groupId);

                                if(dataBase.getException())
                                    Toast.makeText(getApplication(), getString(R.string.connectionError), Toast.LENGTH_SHORT).show();

                                else
                                {
                                    costDialog.dismiss();
                                    Toast.makeText(getApplication(), "Zaktualizowano koszty! Nowy koszt: " + costNew + " zł", Toast.LENGTH_SHORT).show();
                                }
                            }
                        }
                    }
                }
            });

            costDialog.setButton(Dialog.BUTTON_NEUTRAL, "Anuluj", new DialogInterface.OnClickListener()
            {
                @Override
                public void onClick(DialogInterface dialog, int which)
                {
                    costDialog.dismiss();
                }
            });

            costDialog.show();
        }
    }

    public void prepareDateRemoveDialog(final String date)
    {
        final AlertDialog dateRemoveDialog = new AlertDialog.Builder(GroupsMemberActivity.this).create();
        dateRemoveDialog.setMessage("Czy chcesz usunąć termin: " + date + " ?");

        dateRemoveDialog.setButton(Dialog.BUTTON_POSITIVE, "TAK", new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialog, int which)
            {
                String startDate = date.substring(0,16) + ":00.0";
                String endDate = date.substring(19) + ":00.0";

                dataBase.query("delete from terminy where ID_grupy=" + groupId + " and ID_uzytkownika=" + Session.getSession() +
                        " and Termin='" + startDate + "' and Koniec='" + endDate + "'");

                if(dataBase.getException())
                    Toast.makeText(getApplication(), getString(R.string.connectionError), Toast.LENGTH_SHORT).show();

                else
                {
                    updateDatesData();
                    dateRemoveDialog.dismiss();
                    Toast.makeText(getApplication(), "Usunięto termin!", Toast.LENGTH_SHORT).show();
                }
            }
        });

        dateRemoveDialog.setButton(Dialog.BUTTON_NEGATIVE, "NIE", new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialog, int which)
            {
                dateRemoveDialog.dismiss();
            }
        });

        dateRemoveDialog.show();
    }

    public void prepareIntent(String userLogin)
    {
        String userId = dataBase.query("select ID_uzytkownika from użytkownicy where Login='" + userLogin + "'");

        Intent intent = new Intent(this, ProfilesActivity.class);
        intent.putExtra("userId", userId);

        if(dataBase.getException())
            Toast.makeText(this, getString(R.string.connectionError), Toast.LENGTH_SHORT).show();

        else
        {
            if(userId.equals(Session.getSession()))
                intent.setClassName(this, getPackageName() + ".ProfilesOwnActivity");

            else
            {
                String isFriend = dataBase.query("select ID_uzytkownika from kontakty where ID_uzytkownika=" +
                        userId + " and ID_kontaktu=" + Session.getSession());

                if(dataBase.getException())
                    Toast.makeText(this, getString(R.string.connectionError), Toast.LENGTH_SHORT).show();

                else if(isFriend != null)
                    intent.setClassName(this, getPackageName() + ".ProfilesBuddyActivity");
            }

            startActivity(intent);
        }
    }
}
