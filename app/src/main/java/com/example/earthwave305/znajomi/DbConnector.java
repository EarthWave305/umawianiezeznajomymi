package com.example.earthwave305.znajomi;

import android.os.StrictMode;
import android.util.Log;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

/*
Poniższa klasa ma na celu ułatwiać szybki dostęp do bazy SQL. Najistotniejsze metody i sposób użycia:
- String query(String)
    ->  Zwraca pojedyncze komórki danych. Warto używać dla zapytań generujących
        pojedyncze odpowiedzi.

- ArrayList<String> rowQuery(int, String)
    ->  Zwraca listę stringów zawierającą int kolumn z JEDNEGO wiersza. Warto używać jeśli
        potrzebujemy kilku danych z konkretnego rekordu. Z listy można wyłuskać metodą get(int)

- Arraylist<String> columnQuery(String)
    ->  Zwraca listę stringów zawierającą pojedyczną kolumnę danych z WSZYSKICH wierszy, które
        wystąpiły w odpowiedzi serwera SQL. Przydatne jeśli potrzebujemy stworzyć jakąś listę (np. znajomych)

- boolean getException()
    ->  Metoda ma pozwolić na sprawdzenie, czy nie wystapiły jakieś wyjątki w trakcie wykonywania zapytania jedną
        z powyższych metod (niepoprawne zapytanie SQL, nieudane połączenie z serwerem bazy danych, itp.)
        Jeśli takowe wystąpiły to przy pomocy tej metody możemy dać znać o tym użytkownikowi.
*/


public class DbConnector
{
    private Connection connection;

    private static final String DEFAULT_DRIVER = "com.mysql.jdbc.Driver";
    private static final String DEFAULT_URL = "jdbc:mysql://192.168.43.142:3306/psim";
    private static final String DEFAULT_USERNAME = "test";
    private static final String DEFAULT_PASSWORD = "test";

    String recentResult = null;
    boolean exception = false; //ta zmienna okresla czy podczas wykonania zapytania wystapily wyjatki (false - nie, true - tak)

    //Ponizsza funkcja ma za zadanie zwracać POJEDYNCZA komorke danych lub wprowadzanie/usuwanie danych!!!
    public String query(String query)//query to zapytanie uzytkownika do bazy SQL
    {
        strictModeConf();

        recentResult = null; //zerowanie ostatniego wyniku
        exception = false; //zerowanie wystapienia wyjatku

        try //obsluga wyjatkow
        {
            this.connection = createConnection(); //polaczenie sie z baza
            Statement statement = connection.createStatement(); //stworzenie zmiennej zapytanie dla ustanowionego polaczenia

            if(query.toLowerCase().startsWith("select"))
            {
                ResultSet result = statement.executeQuery(query); //przeslanie zapytania i zapisanie rezultatu

                while(result.next()) //next() po kolei sprawdza kazdy wiersz odpowiedzi serwera sql
                    recentResult = result.getString(1); //zawsze zwraca tylko jedna (pierwsza) kolumne
            }

            else
                statement.execute(query);

            connection.close(); //zamkniecie polaczenia
        }

        catch (Exception e)
        {
            Log.d("WYJĄTEK:", "Błąd połączenia");
            exception = true;
        }
        //Koniec obslugi wyjatow

        return recentResult;
    }

    //Ponizsza funkcja ma za zadanie zwracac WSZYSTKIE otrzymane w odpowiedzi od serwera SQL kolumny (ale tylko JEDEN wiersz)!!!
    public ArrayList<String> rowQuery(int columns, String query)//query to zapytanie uzytkownika do bazy SQL
    {
        strictModeConf();

        ArrayList<String> resultList = new ArrayList<String>(1);

        exception = false; //zerowanie wystapienia wyjatku

        try //obsluga wyjatkow
        {
            this.connection = createConnection(); //polaczenie sie z baza
            Statement statement = connection.createStatement(); //stworzenie zmiennej zapytanie dla ustanowionego polaczenia
            ResultSet result = statement.executeQuery(query); //przeslanie zapytania i zapisanie rezultatu

            while(result.next()) //next() po kolei sprawdza kazdy wiersz odpowiedzi serwera sql
                for(int counter = 1; counter <= columns; counter++)
                    resultList.add(result.getString(counter));

            connection.close(); //zamkniecie polaczenia
        }

        catch (Exception e)
        {
            e.printStackTrace();
            Log.d("WYJĄTEK:", "Błąd połączenia");
            exception = true;
        }
        //Koniec obslugi wyjatow

        return resultList;
    }

    //Ponizsza funkcja ma za zadanie zwracac WSZYSTKIE otrzymane w odpowiedzi od serwera SQL wiersze (ale tylko JEDNA kolumne)!!!
    public ArrayList<String> columnQuery(String query)//query to zapytanie uzytkownika do bazy SQL
    {
        strictModeConf();

        ArrayList<String> resultList = new ArrayList<String>(1);
        exception = false; //zerowanie wystapienia wyjatku

        try //obsluga wyjatkow
        {
            this.connection = createConnection(); //polaczenie sie z baza
            Statement statement = connection.createStatement(); //stworzenie zmiennej zapytanie dla ustanowionego polaczenia
            ResultSet result = statement.executeQuery(query); //przeslanie zapytania i zapisanie rezultatu

            while(result.next()) //next() po kolei sprawdza kazdy wiersz odpowiedzi serwera sql
                resultList.add(result.getString(1));

            connection.close(); //zamkniecie polaczenia
        }

        catch (Exception e)
        {
            e.printStackTrace();
            Log.d("WYJĄTEK:", "Błąd połączenia");
            exception = true;
        }
        //Koniec obslugi wyjatow

        return resultList;
    }

    public boolean getException() //funkcja zwraca wystapienie wyjatku
    {
        return exception;
    }

    public String getRecentResult()
    {
        return recentResult;
    }

    public void strictModeConf()
    {
        if (android.os.Build.VERSION.SDK_INT > 9)
        {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);//To odpowiada za watek bez ktorego metoda nie zadziala
        }//Mozna ewentualnie uzyc watku przy wywolaniu tej metody :D, wtedy to nie bedzie potrzebne
    }

    public Connection createConnection(String driver, String url, String username, String password) throws ClassNotFoundException, java.sql.SQLException
    {
        Class.forName(driver); //laduje sterowniki bazy danych dla DriverManagera
        DriverManager.setLoginTimeout(3);
        return DriverManager.getConnection(url, username, password); //sterownik wywoluje polaczenie z baza
    }

    public Connection createConnection() throws ClassNotFoundException, java.sql.SQLException
    {
        //podlaczenie do bazy przy pomocy konkretnych danych
        return createConnection(DEFAULT_DRIVER, DEFAULT_URL, DEFAULT_USERNAME, DEFAULT_PASSWORD);
    }
}
