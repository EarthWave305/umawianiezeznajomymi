package com.example.earthwave305.znajomi;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.InputFilter;
import android.text.InputType;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class ProfilesOwnActivity extends AppCompatActivity
{
    ArrayList<String> userData;

    TextView loginTextView;
    TextView nameTextView;
    TextView nameValueTextView;
    TextView surnameTextView;
    TextView surnameValueTextView;
    TextView mailTextView;
    TextView mailValueTextView;

    ImageButton nameButton;
    ImageButton surnameButton;
    ImageButton mailButton;
    Button passButton;

    DbConnector dataBase;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profiles_own);

        userData = new ArrayList<String>(1);

        loginTextView = (TextView) findViewById(R.id.loginTextView);
        nameTextView = (TextView) findViewById(R.id.nameTextView);
        nameValueTextView = (TextView) findViewById(R.id.nameValueTextView);
        surnameTextView = (TextView) findViewById(R.id.surnameTextView);
        surnameValueTextView = (TextView) findViewById(R.id.surnameValueTextView);
        mailTextView = (TextView) findViewById(R.id.mailTextView);
        mailValueTextView = (TextView) findViewById(R.id.mailValueTextView);

        nameButton = (ImageButton) findViewById(R.id.nameButton);
        surnameButton = (ImageButton) findViewById(R.id.surnameButton);
        mailButton = (ImageButton) findViewById(R.id.mailButton);
        passButton = (Button) findViewById(R.id.passButton);

        dataBase = new DbConnector();

        setUpInterface();
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    public void setUpInterface()
    {
        userData = dataBase.rowQuery(5, "select Login, Imie, Nazwisko, Mail, Haslo from użytkownicy where ID_uzytkownika=" + Session.getSession());

        if(dataBase.getException())
            Toast.makeText(this, getString(R.string.connectionError), Toast.LENGTH_SHORT).show();

        else
        {
            loginTextView.setText(userData.get(0));
            nameValueTextView.setText(userData.get(1));
            surnameValueTextView.setText(userData.get(2));
            mailValueTextView.setText(userData.get(3));
        }
    }

    public void onClickName(View view)
    {
        final AlertDialog editNameDialog = new AlertDialog.Builder(this).create();
        editNameDialog.setMessage("Wprowadź nowe imię");

        final EditText input = new EditText(ProfilesOwnActivity.this);
        input.setHint(getResources().getString(R.string.nameLengthHint));
        input.setFilters(new InputFilter[] {new InputFilter.LengthFilter(getResources().getInteger(R.integer.nameMaxLength))});
        editNameDialog.setView(input);

        editNameDialog.setButton(Dialog.BUTTON_NEGATIVE, "Anuluj", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                editNameDialog.dismiss();
            }
        });

        editNameDialog.setButton(Dialog.BUTTON_POSITIVE, "Zatwierdź", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which)
            {
                String name = input.getText().toString();

                if(name.length() < getResources().getInteger(R.integer.nameMinLength) || name.equals(userData.get(1)))
                    Toast.makeText(getApplication(), "Niepoprawna długość!", Toast.LENGTH_SHORT).show();

                else
                {
                    dataBase.query("update użytkownicy set Imie='" + name + "' where ID_uzytkownika=" + Session.getSession());

                    if(dataBase.getException())
                        Toast.makeText(getApplication(), getResources().getString(R.string.connectionError), Toast.LENGTH_SHORT).show();

                    else
                    {
                        nameValueTextView.setText(name);
                        editNameDialog.dismiss();
                        Toast.makeText(getApplication(), "Zmieniono imię!", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
        editNameDialog.show();
    }

    public void onClickSurname(View view)
    {
        final AlertDialog editSurnameDialog = new AlertDialog.Builder(this).create();
        editSurnameDialog.setMessage("Wprowadź nowe nazwisko");

        final EditText input = new EditText(ProfilesOwnActivity.this);
        input.setHint(getResources().getString(R.string.surnameLengthHint));
        input.setFilters(new InputFilter[] {new InputFilter.LengthFilter(getResources().getInteger(R.integer.surnameMaxLength))});
        editSurnameDialog.setView(input);

        editSurnameDialog.setButton(Dialog.BUTTON_NEGATIVE, "Anuluj", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                editSurnameDialog.dismiss();
            }
        });

        editSurnameDialog.setButton(Dialog.BUTTON_POSITIVE, "Zatwierdź", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which)
            {
                String surname = input.getText().toString();

                if(surname.length() < getResources().getInteger(R.integer.surnameMinLength) || surname.equals(userData.get(2)))
                    Toast.makeText(getApplication(), "Niepoprawna długość!", Toast.LENGTH_SHORT).show();

                else
                {
                    dataBase.query("update użytkownicy set Nazwisko='" + surname + "' where ID_uzytkownika=" + Session.getSession());

                    if(dataBase.getException())
                        Toast.makeText(getApplication(), getResources().getString(R.string.connectionError), Toast.LENGTH_SHORT).show();

                    else
                    {
                        surnameValueTextView.setText(surname);
                        editSurnameDialog.dismiss();
                        Toast.makeText(getApplication(), "Zmieniono nazwisko!", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
        editSurnameDialog.show();
    }

    public void onClickMail(View view)
    {
        final AlertDialog editMailDialog = new AlertDialog.Builder(this).create();
        editMailDialog.setMessage("Wprowadź nowy adres e-mail");

        final EditText input = new EditText(ProfilesOwnActivity.this);
        input.setHint(getResources().getString(R.string.mailLengthHint));
        input.setFilters(new InputFilter[] {new InputFilter.LengthFilter(getResources().getInteger(R.integer.mailMaxLength))});
        editMailDialog.setView(input);

        editMailDialog.setButton(Dialog.BUTTON_NEGATIVE, "Anuluj", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                editMailDialog.dismiss();
            }
        });

        editMailDialog.setButton(Dialog.BUTTON_POSITIVE, "Zatwierdź", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which)
            {
                String mail = input.getText().toString();

                if(mail.length() < getResources().getInteger(R.integer.mailMinLength) || mail.equals(userData.get(3)))
                    Toast.makeText(getApplication(), "Niepoprawna długość!", Toast.LENGTH_SHORT).show();

                else
                {
                    String mailAvailability = dataBase.query("select ID_uzytkownika from użytkownicy where Mail='" + mail + "'");

                    if(dataBase.getException())
                        Toast.makeText(getApplication(), getResources().getString(R.string.connectionError), Toast.LENGTH_SHORT).show();

                    else if(mailAvailability != null)
                        Toast.makeText(getApplication(), "Wprowadzony e-mail jest już zajęty!", Toast.LENGTH_SHORT).show();

                    else
                    {
                        dataBase.query("update użytkownicy set Mail='" + mail + "' where ID_uzytkownika=" + Session.getSession());

                        if(dataBase.getException())
                            Toast.makeText(getApplication(), getResources().getString(R.string.connectionError), Toast.LENGTH_SHORT).show();

                        else
                        {
                            mailValueTextView.setText(mail);
                            editMailDialog.dismiss();
                            Toast.makeText(getApplication(), "Zmieniono adres e-mail!", Toast.LENGTH_SHORT).show();
                        }
                    }
                }
            }
        });
        editMailDialog.show();
    }

    public void onClickPass(View view)
    {
        final AlertDialog editPassDialog = new AlertDialog.Builder(this).create();
        editPassDialog.setMessage("Wprowadź aktualne i nowe hasło");

        final EditText input = new EditText(ProfilesOwnActivity.this);
        input.setHint("Nowe hasło (" + getResources().getString(R.string.passLengthHint) + ")");
        input.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
        input.setFilters(new InputFilter[] {new InputFilter.LengthFilter(getResources().getInteger(R.integer.passMaxLength))});

        final EditText old = new EditText(ProfilesOwnActivity.this);
        old.setHint("Aktualne hasło");
        old.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
        old.setFilters(input.getFilters());

        LinearLayout layout = new LinearLayout(ProfilesOwnActivity.this);
        layout.setOrientation(LinearLayout.VERTICAL);
        layout.addView(old);
        layout.addView(input);
        editPassDialog.setView(layout);

        editPassDialog.setButton(Dialog.BUTTON_NEGATIVE, "Anuluj", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                editPassDialog.dismiss();
            }
        });

        editPassDialog.setButton(Dialog.BUTTON_POSITIVE, "Zatwierdź", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which)
            {
                String pass = input.getText().toString();
                String oldPass = old.getText().toString();

                if(pass.length() < getResources().getInteger(R.integer.passMinLength) || pass.equals(userData.get(4)))
                    Toast.makeText(getApplication(), "Niepoprawna długość!", Toast.LENGTH_SHORT).show();

                else if(!oldPass.equals(userData.get(4)))
                    Toast.makeText(getApplication(), "Wprowadzone aktualne hasło jest niepoprawne!", Toast.LENGTH_SHORT).show();

                else
                {
                    dataBase.query("update użytkownicy set Haslo='" + pass + "' where ID_uzytkownika=" + Session.getSession());

                    if(dataBase.getException())
                        Toast.makeText(getApplication(), getResources().getString(R.string.connectionError), Toast.LENGTH_SHORT).show();

                    else
                    {
                        editPassDialog.dismiss();
                        Toast.makeText(getApplication(), "Zmieniono hasło!", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
        editPassDialog.show();
    }
}

