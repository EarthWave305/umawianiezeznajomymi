package com.example.earthwave305.znajomi;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

/**
 * Created by EarthWave305 on 2017-04-23.
 */

public class LobbyContactsFragment extends Fragment
{
    ArrayList<String> contactsList;
    ArrayList<String> sentList;
    ArrayList<String> invitationsList;

    ListView contactsListView;
    ListView sentListView;
    ListView invitationsListView;
    EditText addEditText;
    ImageButton addButton;
    ImageButton refreshButton;
    Button userProfileButton;

    DbConnector dataBase;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        final View rootView = inflater.inflate(R.layout.fragment_lobby_contacts, container, false);

        contactsListView = (ListView) rootView.findViewById(R.id.contactsListView);
        sentListView = (ListView) rootView.findViewById(R.id.sentListView);
        invitationsListView = (ListView) rootView.findViewById(R.id.invitationsListView);
        addEditText = (EditText) rootView.findViewById(R.id.addEditText);
        addButton = (ImageButton) rootView.findViewById(R.id.addButton);
        refreshButton = (ImageButton) rootView.findViewById(R.id.refreshButton);
        userProfileButton = (Button) rootView.findViewById(R.id.userProfileButton);

        dataBase = new DbConnector();
        contactsList = new ArrayList<String>(1);
        sentList = new ArrayList<String>(1);
        invitationsList = new ArrayList<String>(1);

        updateContactsList();
        updateInvitationsList();
        updateSentList();

        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClickAdd(v);
            }
        });

        addButton.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                onLongClickAdd(v);
                return true;
            }
        });

        refreshButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateContactsList();
                updateSentList();
                updateInvitationsList();
            }
        });

        userProfileButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), ProfilesOwnActivity.class);
                startActivity(intent);
            }
        });

        contactsListView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                prepareDeleteDialog(contactsListView.getItemAtPosition(position).toString());
                return true;
            }
        });

        invitationsListView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                prepareInvitationsDialog(invitationsListView.getItemAtPosition(position).toString());
                return true;
            }
        });

        sentListView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                prepareSentDialog(sentListView.getItemAtPosition(position).toString());
                return true;
            }
        });

        contactsListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                prepareIntent(contactsListView.getItemAtPosition(position).toString());
            }
        });

        sentListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                prepareIntent(sentListView.getItemAtPosition(position).toString());
            }
        });

        invitationsListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                prepareIntent(invitationsListView.getItemAtPosition(position).toString());
            }
        });

        return rootView;
    }

    public void updateContactsList()
    {
        contactsList = dataBase.columnQuery("select u.Login from użytkownicy u join" +
                " kontakty k on u.ID_uzytkownika=k.ID_kontaktu and k.ID_uzytkownika=" + Session.getSession());

        if(dataBase.getException())
            Toast.makeText(getActivity(), getString(R.string.connectionError), Toast.LENGTH_SHORT).show();

            contactsListView.setAdapter(new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, contactsList));
    }

    public void updateSentList()
    {
        sentList = dataBase.columnQuery("select u.Login from użytkownicy u join zaproszenia z on u.ID_uzytkownika=z.ID_celu" +
            " and z.ID_wysylajacego=" + Session.getSession());

        if(dataBase.getException())
            Toast.makeText(getActivity(), getString(R.string.connectionError), Toast.LENGTH_SHORT).show();

            sentListView.setAdapter(new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, sentList));
    }

    public void updateInvitationsList()
    {
        invitationsList = dataBase.columnQuery("select u.Login from użytkownicy u join zaproszenia z on u.ID_uzytkownika=z.ID_wysylajacego" +
                " and z.ID_celu=" + Session.getSession());

        if(dataBase.getException())
            Toast.makeText(getActivity(), getString(R.string.connectionError), Toast.LENGTH_SHORT).show();

            invitationsListView.setAdapter(new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, invitationsList));
    }

    public void onClickAdd(View view)
    {
        String newFriendLogin = addEditText.getText().toString();
        updateInvitationsList();
        updateSentList();

        if (newFriendLogin.length() < getResources().getInteger(R.integer.loginMinLength))
            Toast.makeText(getActivity(), "Login musi mieć minimum " + getString(R.string.loginMinLength) +
                    " znaków długości!", Toast.LENGTH_SHORT).show();

        else if(invitationsList.contains(newFriendLogin) || sentList.contains(newFriendLogin))
            Toast.makeText(getActivity(), "Nie można wysłać zaproszenia! Zaproszenie mogło zostać wysłane już wcześniej!", Toast.LENGTH_SHORT).show();

        else
        {
            String newFriendId = dataBase.query("select ID_uzytkownika from użytkownicy where Login='" + newFriendLogin + "'");

            if(dataBase.getException())
                Toast.makeText(getActivity(), getString(R.string.connectionError), Toast.LENGTH_SHORT).show();

            else if(newFriendId == null || newFriendId.equals(Session.getSession()) || contactsList.contains(newFriendLogin))
                Toast.makeText(getActivity(), "Błędna nazwa użytkownika!", Toast.LENGTH_SHORT).show();

            else
            {
                dataBase.query("insert into zaproszenia (ID_wysylajacego,ID_celu) values(" + Session.getSession() + "," + newFriendId + ")");

                if(dataBase.getException())
                    Toast.makeText(getActivity(), getString(R.string.connectionError), Toast.LENGTH_SHORT).show();

                else
                {
                    updateSentList();
                    Toast.makeText(getActivity(), "Wysłano " + newFriendLogin + " zaproszenie do znajomych!", Toast.LENGTH_SHORT).show();
                    addEditText.setText("");
                }
            }
        }
    }

    public void onLongClickAdd(View view)
    {
        SearchFriendFragment searchDialog = new SearchFriendFragment();
        FragmentManager fm = getActivity().getSupportFragmentManager();
        String phrase = addEditText.getText().toString();

        if(phrase.equals(""))
            searchDialog.setPhrase(null);

        else
            searchDialog.setPhrase(phrase);

        searchDialog.show(fm, "searchDialog");
    }

    public void prepareDeleteDialog(final String friendToDelete)
    {
        final AlertDialog friendDeleteDialog = new AlertDialog.Builder(getActivity()).create();
        friendDeleteDialog.setMessage("Czy chcesz usunąć użytkownika " + friendToDelete + " ze znajomych?");

        friendDeleteDialog.setButton(Dialog.BUTTON_POSITIVE, "TAK", new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialog, int which)
            {
                String userToDeleteId = dataBase.query("select ID_uzytkownika from użytkownicy where Login='" + friendToDelete + "'");

                if(dataBase.getException())
                    Toast.makeText(getActivity(), getString(R.string.connectionError), Toast.LENGTH_SHORT).show();

                else
                {
                    dataBase.query("delete from kontakty where ID_uzytkownika=" + Session.getSession() + " and ID_kontaktu=" + userToDeleteId);

                    if(dataBase.getException())
                        Toast.makeText(getActivity(), getString(R.string.connectionError), Toast.LENGTH_SHORT).show();

                    else
                    {
                        dataBase.query("delete from kontakty where ID_uzytkownika=" + userToDeleteId + " and ID_kontaktu=" + Session.getSession());

                        if(dataBase.getException())
                            Toast.makeText(getActivity(), getString(R.string.connectionError), Toast.LENGTH_SHORT).show();

                        else
                        {
                            Toast.makeText(getActivity(), "Usunięto " + friendToDelete + " ze znajomych!", Toast.LENGTH_SHORT).show();
                            updateContactsList();
                        }
                    }

                }
                friendDeleteDialog.dismiss();
            }
        });

        friendDeleteDialog.setButton(Dialog.BUTTON_NEGATIVE, "NIE", new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialog, int which)
            {
                friendDeleteDialog.dismiss();
            }
        });

        friendDeleteDialog.show();
    }

    public void prepareInvitationsDialog(final String invitationsLogin)
    {
        final AlertDialog invitationsDialog = new AlertDialog.Builder(getActivity()).create();
        invitationsDialog.setMessage(invitationsLogin + " zaprosił Cię do znajomych");

        invitationsDialog.setButton(Dialog.BUTTON_POSITIVE, "Akceptuj", new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialog, int which)
            {
                String invitationsId = dataBase.query("select ID_uzytkownika from użytkownicy where Login='" + invitationsLogin + "'");

                if(dataBase.getException())
                    Toast.makeText(getActivity(), getString(R.string.connectionError), Toast.LENGTH_SHORT).show();

                else
                {
                    dataBase.query("insert into kontakty values (" + Session.getSession() + "," + invitationsId + ")" );
                    boolean first = dataBase.getException();

                    dataBase.query("insert into kontakty values (" + invitationsId + "," + Session.getSession() + ")" );
                    boolean second = dataBase.getException();

                    dataBase.query("delete from zaproszenia where ID_wysylajacego=" + invitationsId + " and ID_celu=" + Session.getSession());
                    boolean third = dataBase.getException();

                    if(first || second || third)
                        Toast.makeText(getActivity(), getString(R.string.connectionError), Toast.LENGTH_SHORT).show();

                    else
                    {
                        Toast.makeText(getActivity(), "Dodano " + invitationsLogin + " do znajomych!", Toast.LENGTH_SHORT).show();
                        updateInvitationsList();
                        updateContactsList();
                    }
                }
                invitationsDialog.dismiss();
            }
        });

        invitationsDialog.setButton(Dialog.BUTTON_NEGATIVE, "Odrzuć", new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialog, int which)
            {
                String invitationsId = dataBase.query("select ID_uzytkownika from użytkownicy where Login='" + invitationsLogin + "'");

                if(dataBase.getException())
                    Toast.makeText(getActivity(), getString(R.string.connectionError), Toast.LENGTH_SHORT).show();

                else
                {
                    dataBase.query("delete from zaproszenia where ID_wysylajacego=" + invitationsId + " and ID_celu=" + Session.getSession());

                    if(dataBase.getException())
                        Toast.makeText(getActivity(), getString(R.string.connectionError), Toast.LENGTH_SHORT).show();

                    else
                    {
                        Toast.makeText(getActivity(), "Usunięto zaproszenie użytkownika " + invitationsLogin + " !", Toast.LENGTH_SHORT).show();
                        updateInvitationsList();
                    }
                }
                invitationsDialog.dismiss();
            }
        });

        invitationsDialog.setButton(Dialog.BUTTON_NEUTRAL, "Zamknij", new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialog, int which)
            {
                invitationsDialog.dismiss();
            }
        });

        invitationsDialog.show();
    }

    public void prepareSentDialog(final String sentLogin)
    {
        final AlertDialog sentDialog = new AlertDialog.Builder(getActivity()).create();
        sentDialog.setMessage("Zaproszenie wysłane do " + sentLogin + " czeka na akceptację");

        sentDialog.setButton(Dialog.BUTTON_POSITIVE, "Usuń", new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialog, int which)
            {
                String sentId = dataBase.query("select ID_uzytkownika from użytkownicy where Login='" + sentLogin + "'");

                if(dataBase.getException())
                    Toast.makeText(getActivity(), getString(R.string.connectionError), Toast.LENGTH_SHORT).show();

                else
                {
                    dataBase.query("delete from zaproszenia where ID_wysylajacego=" + Session.getSession() + " and ID_celu=" + sentId);

                    if(dataBase.getException())
                        Toast.makeText(getActivity(), getString(R.string.connectionError), Toast.LENGTH_SHORT).show();

                    else
                    {
                        Toast.makeText(getActivity(), "Anulowano zaproszenie!", Toast.LENGTH_SHORT).show();
                        updateSentList();
                    }
                }
                sentDialog.dismiss();
            }
        });

        sentDialog.setButton(Dialog.BUTTON_NEGATIVE, "Zamknij", new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialog, int which)
            {
                sentDialog.dismiss();
            }
        });

        sentDialog.show();
    }

    public void prepareIntent(String userLogin)
    {
        String userId = dataBase.query("select ID_uzytkownika from użytkownicy where Login='" + userLogin + "'");

        Intent intent = new Intent(getActivity(), ProfilesActivity.class);
        intent.putExtra("userId", userId);

        if(dataBase.getException())
            Toast.makeText(getActivity(), getString(R.string.connectionError), Toast.LENGTH_SHORT).show();

        else
        {
            if(userId.equals(Session.getSession()))
                intent.setClassName(getActivity(), getContext().getPackageName() + ".ProfilesOwnActivity");

            else
            {
                String isFriend = dataBase.query("select ID_uzytkownika from kontakty where ID_uzytkownika=" +
                        userId + " and ID_kontaktu=" + Session.getSession());

                if(dataBase.getException())
                    Toast.makeText(getActivity(), getString(R.string.connectionError), Toast.LENGTH_SHORT).show();

                else if(isFriend != null)
                    intent.setClassName(getActivity(), getContext().getPackageName() + ".ProfilesBuddyActivity");
            }

            startActivity(intent);
        }
    }
}
